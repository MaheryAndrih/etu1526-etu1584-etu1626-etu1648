package Main.repository;

import Main.model.Kilometrage;
import Main.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KilometrageRepository extends JpaRepository<Kilometrage, Long> {
    Kilometrage findByIdavion(int idavion);
}
