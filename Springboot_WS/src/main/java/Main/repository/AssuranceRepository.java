package Main.repository;

import Main.model.Assurance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AssuranceRepository extends JpaRepository<Assurance, Long> {
   /* @Query("select * from assurance where expiration <  (SELECT current_date + interval '3 month')")
    List<Assurance> fetchAllAssignments();*/
}
