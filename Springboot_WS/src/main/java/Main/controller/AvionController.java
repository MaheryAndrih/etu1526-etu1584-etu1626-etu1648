package Main.controller;
import Main.repository.AvionRepository;
import Main.exception.AvionNotFoundException;
import Main.model.Erreur;
import Main.model.JsonDataSuccess;
import Main.model.JsonError;
import Main.model.Avion;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin("*")
@RestController
public class AvionController {

    private final AvionRepository repository;

    public AvionController(AvionRepository repository){
        this.repository=repository;
    }
    @GetMapping("/avion")
    Object all(){
        try{
            JsonDataSuccess<Avion> data = new JsonDataSuccess();
            data.setData(repository.findAll());
            return data;
        }
        catch(Exception ex) {
            JsonError err = new JsonError();
            Erreur e = new Erreur();
            e.setCode(404);
            e.setMessage("Error found");
            err.setError(e);
            return err;
        }
    }
    @PostMapping("/avion")
    Avion newEmployee(@RequestBody Avion newAvion) {
        return repository.save(newAvion);
    }
    @GetMapping("/avion/{id}")
    Object one(@PathVariable Long id) throws Exception{
        try{
            JsonDataSuccess<Avion> data = new JsonDataSuccess();
            data.setData(repository.findById(id)
                    .orElseThrow(() -> new AvionNotFoundException(id)));
            return data;
        }catch(Exception ex){
            JsonError err = new JsonError();
            Erreur e = new Erreur();
            e.setCode(404);
            e.setMessage("ID not found");
            err.setError(e);
            return err;
        }
    }
    @PutMapping("/avion/{id}")
    Object replaceVehicule(@RequestBody Avion newAvion, @PathVariable Long id) {
        try{
            JsonDataSuccess<Avion> data = new JsonDataSuccess();
            data.setData(repository.findById(id)
                    .map(avion -> {
                        avion.setMatricule(newAvion.getMatricule());
                        avion.setNomappareil(newAvion.getNomappareil());
                        return repository.save(avion);
                    })
                    .orElseGet(() -> {
                        newAvion.setIdavion(id);
                        return repository.save(newAvion);
                    }));
            return data;
        }
        catch(Exception ex){
            JsonError err = new JsonError();
            Erreur e = new Erreur();
            e.setCode(404);
            e.setMessage("ID not found");
            err.setError(e);
            return err;
        }
    }
    @DeleteMapping("/avion/{id}")
    public void deleteavion(@PathVariable Long id) {
        repository.deleteById(id);
    }

    /*@GetMapping("/test")
    public String hello(@RequestParam(value = "name", defaultValue = "Mahery") String name) {
        return String.format("Hello %s!", name);
    }*/


}
