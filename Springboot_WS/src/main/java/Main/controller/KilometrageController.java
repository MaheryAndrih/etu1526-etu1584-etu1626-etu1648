package Main.controller;
import Main.repository.KilometrageRepository;
import Main.exception.KilometrageNotFoundException;
import Main.model.Erreur;
import Main.model.JsonDataSuccess;
import Main.model.JsonError;
import Main.model.Kilometrage;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@CrossOrigin("*")
@RestController
public class KilometrageController {
    private final KilometrageRepository repository;

    public KilometrageController(KilometrageRepository repository){
        this.repository=repository;
    }
    @GetMapping("/kilometrages")
    Object all(){
        try{
            JsonDataSuccess<Kilometrage> data = new JsonDataSuccess();
            data.setData(repository.findAll());
            return data;
        }
        catch(Exception ex){
            JsonError err = new JsonError();
            Erreur e = new Erreur();
            e.setCode(404);
            e.setMessage("Error found");
            err.setError(e);
            return err;
        }
    }
    @PostMapping("/kilometrages")
    Kilometrage newKilometrage(@RequestBody Kilometrage newKilometrage) {
        return repository.save(newKilometrage);
    }

    @GetMapping("/kilometrages/{idavion}")
    Object one(@PathVariable int idavion) throws Exception{
        try{
            JsonDataSuccess<Kilometrage> data = new JsonDataSuccess();
            data.setData(repository.findByIdavion(idavion));
            return data;
        }catch(Exception ex){
            JsonError err = new JsonError();
            Erreur e = new Erreur();
            e.setCode(404);
            e.setMessage("ID not found");
            err.setError(e);
            return err;
        }
    }
    @DeleteMapping("/kilometrages/{id}")
    public void deleteKilometrage(@PathVariable Long id) {
        repository.deleteById(id);
    }


}
