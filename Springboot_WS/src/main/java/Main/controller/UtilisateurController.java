package Main.controller;
import Main.model.*;
import Main.repository.KilometrageRepository;
import Main.exception.KilometrageNotFoundException;
import Main.repository.UtilisateurRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Repository
@RestController
public class UtilisateurController {
    private final UtilisateurRepository repository;

    public UtilisateurController(UtilisateurRepository repository){
        this.repository=repository;
    }

    @PostMapping("/Utilisateur/login")
    Object login (@RequestBody Utilisateur user){
        try{
            JsonDataSuccess<Utilisateur> data = new JsonDataSuccess();
            Utilisateur u = repository.findByLoginAndMotdepasse(user.getLogin(),user.getMotdepasse());
            if(u==null)
                data.setData("Login invalid");
            else
                data.setData(u);
            return data;
        }catch(Exception ex){
            JsonError err = new JsonError();
            Erreur e = new Erreur();
            e.setCode(404);
            e.setMessage("Error found");
            err.setError(e);
            return err;
        }
    }
}
