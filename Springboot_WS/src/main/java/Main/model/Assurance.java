package Main.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Date;

@Entity

public class Assurance {
    private @Id @GeneratedValue
    Long idassurance;
    private int idavion    ;
    private Date expiration ;

    public Assurance() {
    }

    public Assurance(Long idassurance, int idavion, Date expiration) {
        this.idassurance = idassurance;
        this.idavion = idavion;
        this.expiration = expiration;
    }

    public Long getIdassurance() {
        return idassurance;
    }

    public void setIdassurance(Long idassurance) {
        this.idassurance = idassurance;
    }

    public int getIdavion() {
        return idavion;
    }

    public void setIdavion(int idavion) {
        this.idavion = idavion;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
}
