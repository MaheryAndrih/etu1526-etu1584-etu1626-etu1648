package Main.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Avion {
    private @Id @GeneratedValue
    Long idavion;
    private String matricule;
    private String nomappareil;

    public Avion() {
    }

    public Avion(Long idavion, String matricule, String nomappareil) {
        this.idavion = idavion;
        this.matricule = matricule;
        this.nomappareil = nomappareil;
    }

    public Long getIdavion() {
        return idavion;
    }

    public void setIdavion(Long idavion) {
        this.idavion = idavion;
    }

    public String getMatricule() {
        return matricule;
    }

    public  void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getNomappareil() {
        return nomappareil;
    }

    public void setNomappareil(String nom_appareil) {
        this.nomappareil = nom_appareil;
    }
}
