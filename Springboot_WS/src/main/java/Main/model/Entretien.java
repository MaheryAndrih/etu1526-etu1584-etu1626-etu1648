package Main.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Date;

@Entity

public class Entretien {
    private @Id @GeneratedValue
    Long identretien;
    private Date dateentretien;
    private String motif;
    private int idavion;
}
