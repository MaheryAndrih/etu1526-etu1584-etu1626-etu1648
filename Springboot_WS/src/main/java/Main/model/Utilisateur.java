package Main.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Utilisateur {

    private @Id @GeneratedValue
    Long id_user;

    private String nomuser;
    private String login  ;
    private String  motdepasse  ;

    public Utilisateur() {
    }

    public Utilisateur(Long id_user, String nomuser, String login, String motdepasse) {
        this.id_user = id_user;
        this.nomuser = nomuser;
        this.login = login;
        this.motdepasse = motdepasse;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public String getNomuser() {
        return nomuser;
    }

    public void setNomuser(String nom_user) {
        this.nomuser = nom_user;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }
}
