package Main.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Date;

@Entity
public class Kilometrage {
    private @Id @GeneratedValue
    Long idkilometrage;
    private int debutkm;
    private int finkm;
    private Date datekm;
    private int idavion;

    public Kilometrage() {
    }

    public Kilometrage(Long idkilometrage, int debutkm, int finkm, Date datekm, int idavion) {
        this.idkilometrage = idkilometrage;
        this.debutkm = debutkm;
        this.finkm = finkm;
        this.datekm = datekm;
        this.idavion = idavion;
    }

    public Long getIdkilometrage() {
        return idkilometrage;
    }

    public void setIdkilometrage(Long idkilometrage) {
        this.idkilometrage = idkilometrage;
    }

    public int getDebutkm() {
        return debutkm;
    }

    public void setDebutkm(int debut_km) {
        this.debutkm = debut_km;
    }

    public int getFinkm() {
        return finkm;
    }

    public void setFinkm(int fin_km) {
        this.finkm = fin_km;
    }

    public Date getDatekm() {
        return datekm;
    }

    public void setDatekm(Date date_km) {
        this.datekm = date_km;
    }

    public int getIdavion() {
        return idavion;
    }

    public void setIdavion(int id_avion) {
        this.idavion = id_avion;
    }
}
