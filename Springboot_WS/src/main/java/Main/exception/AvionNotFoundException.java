package Main.exception;

public class AvionNotFoundException extends RuntimeException{
    public Long id;

    public AvionNotFoundException(Long id){
        super("The Avion with the id"+id+" does not exist");
    }
}
