package Main.exception;

public class KilometrageNotFoundException extends RuntimeException{
    public Long id;

    public KilometrageNotFoundException(Long id){
        super("The Kilometrage with the id "+id+" does not exist");
    }

    public KilometrageNotFoundException(int  id_avion){
        super("The Kilometrage with the id "+id_avion+" does not exist");
    }
}
