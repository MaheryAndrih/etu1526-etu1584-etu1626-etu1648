package Main.exception;

public class UtilisateurNotFoundException extends RuntimeException{
    public UtilisateurNotFoundException(){
        super("The User login invalid");
    }
}
