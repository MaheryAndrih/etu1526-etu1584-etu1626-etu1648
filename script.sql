/*	TABLES	*/

CREATE  TABLE "public".avion ( 
	idavion              serial ,
	matricule            varchar(30)  NOT NULL  ,
	nom_appareil         varchar(30)  NOT NULL  ,
	avion				 varchar(100),
	CONSTRAINT pk_avion PRIMARY KEY ( idavion )
 );

CREATE  TABLE "public".entretien ( 
	identretien          serial  NOT NULL  ,
	date_entretien       date  NOT NULL  ,
	motif                varchar(255)  NOT NULL  ,
	idavion              integer  NOT NULL  ,
	CONSTRAINT pk_entretien PRIMARY KEY ( identretien )
 );

CREATE  TABLE "public".kilometrage ( 
	idkilometrage        serial,
	debut_km             integer    ,
	fin_km               integer    ,
	date_km              date    ,
	id_avion             integer  NOT NULL  ,
	CONSTRAINT pk_kilometrage PRIMARY KEY ( idkilometrage )
 );

CREATE  TABLE "public".utilisateur ( 
	id_user              serial  NOT NULL  ,
	nom_user             varchar(30)  NOT NULL  ,
	login                varchar(30)  NOT NULL  ,
	motdepasse           varchar(30)  NOT NULL  ,
	CONSTRAINT pk_utilisateur PRIMARY KEY ( id_user )
 );

CREATE  TABLE "public".assurance ( 
	idassurance          serial  ,
	idavion              integer  NOT NULL  ,
	expiration           date  NOT NULL  ,
	CONSTRAINT pk_assurance PRIMARY KEY ( idassurance )
 );

ALTER TABLE "public".assurance ADD CONSTRAINT fk_assurance_avion FOREIGN KEY ( idavion ) REFERENCES "public".avion( idavion ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public".entretien ADD CONSTRAINT fk_entretien_avion FOREIGN KEY ( idavion ) REFERENCES "public".avion( idavion ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public".kilometrage ADD CONSTRAINT fk_kilometrage_avion FOREIGN KEY ( id_avion ) REFERENCES "public".avion( idavion ) ON DELETE CASCADE ON UPDATE CASCADE;



/*	INSERTIONS	*/


INSERT INTO avion(matricule,nom_appareil) VALUES
    ('F-1545','ATR 42','img/atr.jpg'),
    ('F-1289','ATR 42','img/atr2.jpg'),
    ('F-4753','Boeing 737','img/boeing2.jpg');

INSERT INTO kilometrage(debut_km,fin_km,date_km,id_avion) VALUES
    (132700,145300,'2022-12-17',3),
    (129760,132500,'2022-12-18',2),
    (239000,241400,'2022-12-19',1);

INSERT INTO assurance(idavion,expiration) VALUES
    (1,'2023-02-12'),
    (2,'2023-04-13'),
    (3,'2023-01-22');

INSERT INTO entretien(date_entretien,motif,idavion) VALUES
    ('2022-11-30','Vidange',1),
    ('2022-12-01','Changement pneus',3);

INSERT INTO utilisateur(nom_user,login,motdepasse) VALUES
    ('user1','user1','user1'),
    ('user2','user2','user2');
