import { IonButton, IonCheckbox, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { useRef } from 'react';
import ExploreContainer from '../components/ExploreContainer';
import './Login.css';


const Login: React.FC = () => {

  let username = useRef<HTMLIonInputElement>(null);
  let password = useRef<HTMLIonInputElement>(null);

  async function login(e: any) {
    e.preventDefault();
    const personData = {
      username: username.current?.value,
      password: password.current?.value
    }

    const url = 'http://localhost:8080/Utilisateur/login';

    try {
        
        const result = fetch(url, {
            method: "POST",
            mode: "cors" as RequestMode,
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': 'token-value',
            },
            body: JSON.stringify(personData),
        })
        
    } catch (err: any) {
        console.log(err.message);
    }
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <form className="ion-padding" onSubmit={login}>
          <IonItem>
            <IonLabel position="floating">Username</IonLabel>
            <IonInput ref={username}/>
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Password</IonLabel>
            <IonInput type="password" ref={password}/>
          </IonItem>
          <IonButton className="ion-margin-top" type="submit" expand="block">
            Login
          </IonButton>
        </form>
      </IonContent>
    </IonPage>
  );
};

export default Login;
