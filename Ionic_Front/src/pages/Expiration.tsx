import { IonBadge, IonButton, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCheckbox, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonList, IonPage, IonRow, IonSelect, IonSelectOption, IonTabBar, IonTabButton, IonTabs, IonTitle, IonToolbar } from '@ionic/react';
import { calendar, informationCircle, map, personCircle } from 'ionicons/icons';
import { useEffect, useState } from 'react';
import ExploreContainer from '../components/ExploreContainer';
import './Login.css';

const Expiration: React.FC = () => {

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Expiration</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen> 

        <IonCard>
          <IonCardHeader>
              <IonSelect placeholder="Date d'expiration">
                <IonSelectOption value="1">Dans 1 mois</IonSelectOption>
                <IonSelectOption value="3">Dans 3 mois</IonSelectOption>
              </IonSelect>
          </IonCardHeader>

          <IonCardContent>
            <IonButton className="ion-margin-top" type="submit" expand="block">
                Search
            </IonButton>
          </IonCardContent>
        </IonCard>

        <IonCard>
          <IonCardHeader>
            <IonCardTitle>Matricule: 1564</IonCardTitle>
            <IonCardSubtitle>Marque: Nissan</IonCardSubtitle>
          </IonCardHeader>

          <IonCardContent>
            Soratroratra be...
          </IonCardContent>
        </IonCard>

        <IonCard>
          <IonCardHeader>
            <IonCardTitle>Matricule: 7681</IonCardTitle>
            <IonCardSubtitle>Marque: Fiat</IonCardSubtitle>
          </IonCardHeader>

          <IonCardContent>
            Soratroratra be...
          </IonCardContent>
        </IonCard>
        
      </IonContent>
    </IonPage>
  );
};

export default Expiration;
