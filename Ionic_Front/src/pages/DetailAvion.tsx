import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonList, IonPage, IonRow, IonSelect, IonSelectOption, IonTabBar, IonTabButton, IonTabs, IonTitle, IonToolbar } from '@ionic/react';
import { calendar, informationCircle, map, personCircle } from 'ionicons/icons';
import ExploreContainer from '../components/ExploreContainer';
import './Login.css';


const DetailVehicule: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>DetailVehicule</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>

        <IonCard>
          <IonCardHeader>
            <IonCardTitle>Matricule: 5865</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            Kilometrage rehetra.......
          </IonCardContent>
        </IonCard>
        
      </IonContent>
    </IonPage>
  );
};

export default DetailVehicule;
