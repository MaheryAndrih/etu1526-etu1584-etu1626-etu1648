import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonContent, IonHeader, IonItem, IonLabel, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { calendar, informationCircle, map, personCircle } from 'ionicons/icons';
import { useEffect, useState } from 'react';
import ExploreContainer from '../components/ExploreContainer';
import './Login.css';



const ListeVehicule: React.FC = () => {

  const [items, setItems] = useState<any[]>([]);


  useEffect(() => {
    fetch('http://localhost:8080/avion',{ method: "GET" }).then((response)=>response.json()).then((json)=>setItems(json.data));
  },[]);


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Liste Avion</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>

      {items.map((item) => {
          return (
            <IonCard>
              <IonCardHeader>
                <IonCardTitle>Id: {item.idavion}</IonCardTitle>
                <IonCardSubtitle>Matricule: {item.matricule}</IonCardSubtitle>
              </IonCardHeader>

              <IonCardContent>
                  Marque: {item.nomappareil}
              </IonCardContent>
            </IonCard>
          )
        })}

      </IonContent>
    </IonPage>
  );
};

export default ListeVehicule;
